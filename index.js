/*
Git commands:
	npm init -y
	npm install express mongoose
	touch .gitignore >> content node_modules
*/

// dependencies
const express = require("express");
const mongoose = require("mongoose");

// models folder >> task.js
// controllers folder >> taskController.js
// routes folder >> taskRoute.js

const taskRoute = require("./routes/taskRoute.js");
// const taskController = require("./controllers/taskController.js");
// const task = require("./models/task.js");

// Server setup
const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// DB connection
mongoose.connect("mongodb+srv://admin:admin@b218-to-do.8fmkrnm.mongodb.net/toDo?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

// endpoint
app.use("/tasks", taskRoute);

app.listen(port, ()=> console.log(`Now listening to port ${port}`));